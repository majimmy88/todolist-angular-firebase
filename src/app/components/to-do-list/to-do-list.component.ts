import { Component, OnInit } from '@angular/core';
import { ToDoService } from '../../services/to-do.service';
import { map } from 'rxjs/operators';

import { Todo } from '../../models/Todo'


@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {

  todos:Todo[];

  constructor(private todoService:ToDoService) { }

  ngOnInit(): void {
    this.getTodos();
  }

  getTodos() {
    this.todoService.getTodos().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
          )
        )
    ).subscribe(todos => {
      this.todos = todos;
    });
  }

  deleteTodo(todo:Todo) {
    this.todos = this.todos.filter(item => item.key !== todo.key);
    this.todoService.deleteTodo(todo.key)
      .then(() => {
        console.log('Item was successfully deleted');
      })
      .catch (err => console.log(err));
  }

  addTodo(todo:Todo) {
    this.todoService.addTodo(todo).then(() => {
      console.log('Created new item successfully!');
    });
  }
}
