import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ToDoService } from '../../services/to-do.service'

import { Todo } from 'src/app/models/Todo';

@Component({
  selector: 'app-to-do-item',
  templateUrl: './to-do-item.component.html',
  styleUrls: ['./to-do-item.component.css']
})
export class ToDoItemComponent implements OnInit {
  @Input() todo: Todo;
  @Output() deleteTodo: EventEmitter<Todo> = new EventEmitter();

  constructor(private todoService:ToDoService) { }

  ngOnInit(): void {
  }

  setClasses() {
    let classes = {
      todo:true,
      'completed': this.todo.completed
    }

    return classes;
  }

  onToggle(todo) {
    todo.completed = !todo.completed;
    this.todoService.toggleCompleted(todo)
      .then(() =>
      console.log('Item was successfully updated')
      )
      .catch(err => {
        console.log(err)
      })
  }

  onDelete(todo) {
    this.deleteTodo.emit(todo);
  }

}
