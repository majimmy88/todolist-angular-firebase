import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
// import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Todo } from '../models/Todo'
// import { Observable } from 'rxjs';

// const httpOptions = {
//   headers:new HttpHeaders({
//     'Content-Type': 'application/json'
//   })
// }

@Injectable({
  providedIn: 'root'
})
export class ToDoService {

  private dbPath = '/todos';

  todosRef: AngularFireList<Todo> = null;

  constructor(private db: AngularFireDatabase) {
    this.todosRef = db.list(this.dbPath)
  }

  getTodos(): AngularFireList<Todo> {
    return this.todosRef;
  }

  deleteTodo(key: string): Promise<void> {
    return this.todosRef.remove(key)
  }

  addTodo(todo:Todo): any {
    return this.todosRef.push(todo);
  }

  toggleCompleted(todo:Todo): Promise<void> {
    return this.todosRef.update(todo.key, todo)
  }


}
