// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB1YErYfgyLIWgIrh_CoeA-3sPcDwmaRBc",
    authDomain: "todolist-dbeb6.firebaseapp.com",
    databaseURL: "https://todolist-dbeb6.firebaseio.com",
    projectId: "todolist-dbeb6",
    storageBucket: "todolist-dbeb6.appspot.com",
    messagingSenderId: "357988886782",
    appId: "1:357988886782:web:535530c160441c14059daf",
    measurementId: "G-R7851BCYCT"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
